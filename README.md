
* Expected currency rate UAH to USD only.
* User sees a page with a chart of stored currency rates (or an empty page if database is empty). 
* User sees a form to create and save currency rate for specific date. 
* User can set rate and date. All the fields are mandatory.
* User can save entered rate for specific date. After saving chart refreshes.

Technologies stack: 
* Python 3
* Django
* Materialize CSS
* chart.js
* AJAX


### On Ubuntu 18.04 machine, below installations are required:
* `sudo apt-get install gcc libpq-dev -y`
* `sudo apt-get install python-dev  python-pip -y`
* `sudo apt-get install python3-dev python3-pip python3-venv python3-wheel -y`
* `pip3 install wheel`


### Start project
* `git clone https://repo.url` - clone repo
* `python3 -m venv venv` - configure virtual environment
* `. venv/bin/activate` - activate virtual environment
* `pip install -r requirements.txt` - install requirements
* `python manage.py migrate` - migrate database
* `python manage.py loaddata fixture.json` - data generation
* `python manage.py runserver` - run development server
