M.AutoInit(); // initialize Materialize Components


function drow_chart(dates, rates) {
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            "labels": dates,
            "datasets": [{
                "label": "rate",
                "data": rates,
                // "fill":false,
                "borderColor": "rgb(75, 192, 192)",
                "lineTension": 0.1,
                "pointRadius": 0,
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    return myChart;
}

myChart = drow_chart(dates, rates)

function update_chart(chart, dates, rates) {
    chart.data.labels = dates
    chart.data.datasets[0].data = rates
    chart.update();
}


// AJAX ////////////////////////////

$("#save").click(function(e) {
    e.preventDefault()

    $('#rate_error').html("")
    $('#date_error').html("")
    $('#output').html("")

    // console.log("rate", $('#rate').val())
    // console.log("date", $('#date').val())


    if ($('#rate').val() == ""){ // check if form is filled
        $('#rate_error').html("rate is required")
    } else if ($('#date').val() == ""){
        $('#date_error').html("date is required")
    } else {
        $.ajax({
                type : "POST", 
                url: ajax_posting_url,
                data: {
                    rate : $('#rate').val(),
                    date : $('#date').val(),
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
                    dataType: "json",

                },
                
                success: function(data){
                    if (data.is_valid){
                        update_chart(myChart, data.dates, data.rates) // rederaw chart with new data
                        M.toast({html: "Rate was updated ✅", displayLength: 2000}) // pop up massage


                    } else {
                        // display errors
                        if (data.rate_error){ 
                            $('#rate_error').html(data.rate_error)
                        }
                        if (data.date_error){
                            $('#date_error').html(data.date_error)
                        }
                    }
                },

                failure: function() {
                    $('#output').html("an ERROR occured")
                }

            });
    }
});