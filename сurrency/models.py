from django.db import models

# Create your models here.


class Currency_rate(models.Model):
    rate = models.DecimalField(max_digits=5, decimal_places=2)
    date = models.DateField(unique=True)

    def __str__(self):
        return f"{self.rate}: {self.date}"
