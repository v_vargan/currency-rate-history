from django.shortcuts import render
from django.views.generic.list import ListView
from django.http import JsonResponse


from .models import Currency_rate

import json
import datetime 


def genarate_dataset():
    dates, rates = [], []
    queryset = Currency_rate.objects.order_by('date')
    for i in range(len(queryset)-1):
        start_date, start_rate = queryset[i].date, queryset[i].rate 
        end_date, end_rate = queryset[i+1].date, queryset[i+1].rate 
        delta = (end_date - start_date).days      # as timedelta


        if delta > 1: # if there are more than one day beatween timestamps we fill the gap

            dates.append(start_date.strftime("%m.%d.%Y")) # adding start date data because next loop will ignore it
            rates.append(float(start_rate))

            # print("start_date: {}\tend_date: {}\tdelta: {}".format(start_date, end_date, delta))

            day_rate_change = (end_rate - start_rate) / delta # rate change on every day
            day_rate = start_rate

            # print("start_rate: {}\tend_rate: {}\tday_rate_change: {}".format(start_rate, end_rate, day_rate_change))

            for j in range(1, delta):
                day = start_date + datetime.timedelta(days=j)
                day_rate += day_rate_change

                # print("day: {}\tday_rate: {}".format(day, day_rate))

                dates.append(day.strftime("%m.%d.%Y"))
                rates.append(float(round(day_rate, 2)))
        else:
            dates.append(start_date.strftime("%m.%d.%Y"))
            rates.append(float(start_rate))

    # end rate is not included so we add it 
    last_index = len(queryset) - 1 # queryset Negative indexing is not supported in django
    dates.append(queryset[last_index].date.strftime("%m.%d.%Y"))
    dates.append(float(queryset[last_index].rate))

    return dates, rates


class Chart_View(ListView):
    model = Currency_rate

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        dates, rates = genarate_dataset()
        context["dates"] = json.dumps(dates) # need to pack in json to correctly transport to Ninja
        context["rates"] = rates
        return context


# ajax_posting function
def ajax_posting(request):
    if request.is_ajax():
        rate = int(request.POST.get('rate', None))

        date = request.POST.get('date', None)
        date = list(map(int, date.split("-")))
        date = datetime.date(*date)

        # print("rate: ", rate)
        # print("date: ", date)

        response = {"is_valid": True}
        if  not (0 <= float(rate) <= 100):  # check if it's a float form 0 to 100
            response["is_valid"] = False
            response["rate_error"] = "from 0 to 100"
            
        if response["is_valid"]:
            rate_exist = Currency_rate.objects.filter(date=date)
            if rate_exist:
                rate_exist.update(rate=rate)
            else:
                Currency_rate.objects.update_or_create(rate=rate, date=date)
            dates, rates = genarate_dataset()
            response['dates'] = dates
            response['rates'] = rates

        return JsonResponse(response)